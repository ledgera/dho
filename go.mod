module gitlab.com/ledgera/dho

go 1.16

require (
	gitlab.com/ledgera/math v0.0.0-20220403015209-be7ad3d12a21
	gitlab.com/ledgera/sam v0.0.0-20220403015307-3970e848870c
)
